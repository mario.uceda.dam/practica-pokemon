package practicapokemon;
public abstract class Pokemon {
    
    //atributos
    protected String Especie;
    protected String Mote;
    protected String Tipo;
    protected int Nivel;
    protected int Ps;
    protected int Ataque;
    protected int Defensa;
    
    //contructor
    
    public Pokemon (){
        
    }
    
    //métodos
    
    //Getters

    public int getNivel() {
        return Nivel;
    }

    public String getEspecie() {
        return Especie;
    }

    public int getAtaque() {
        return Ataque;
    }

    public int getDefensa() {
        return Defensa;
    }

    public String getMote() {
        if (Mote==null){
            return Especie;
        }
        return Mote;
    }

    public int getPs() {
        return Ps;
    }

    public String getTipo() {
        return Tipo;
    }

    //Setters
    public void setMote(String Mote) {
        this.Mote = Mote;
    }
    
}
