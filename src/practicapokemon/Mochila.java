package practicapokemon;

public class Mochila {
    
    //atributos
    
    protected int contador=0;
    protected Pokemon[] espacio = new Pokemon[100];
    
    //constructor
    
    public Mochila(){

        
    }
    
    //métodos
  
    public void capturarPokemon(Pokemon random){
        espacio[contador]= random;
            contador++;        
    }
    
    public Pokemon mostrarPokemon( int contador){
        
        return espacio[contador];
    }
    
    public void eliminarPokemon(int contador){
        espacio[contador]=null;
    }
    
}
