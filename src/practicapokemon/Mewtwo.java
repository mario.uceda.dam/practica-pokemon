package practicapokemon;
public class Mewtwo extends Pokemon{
    
    public Mewtwo(int Nivel, String Mote){
        this.Nivel=Nivel;
        this.Mote=Mote;
        this.Ataque=Nivel*7;
        this.Defensa=Nivel*5;
        this.Ps=Nivel*8;
        this.Especie="Mewtwo";
        this.Tipo="Psiquico";
    }
    public Mewtwo (int Nivel){
        this.Nivel=Nivel;
        this.Mote=Especie;
        this.Ataque=Nivel*7;
        this.Defensa=Nivel*5;
        this.Ps=Nivel*8;
        this.Especie="Mewtwo";
        this.Tipo="Psiquico";
    }
}