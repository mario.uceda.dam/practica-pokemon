package practicapokemon;
public class Bulbasaur extends Pokemon{
   
    public Bulbasaur (int Nivel, String Mote){
        this.Nivel=Nivel;
        this.Mote=Mote;
        this.Ataque=Nivel*2;
        this.Defensa=Nivel*3;
        this.Ps=Nivel*7;
        this.Especie="Bulbasaur";
        this.Tipo="Planta/Veneno";
    }
    public Bulbasaur (int Nivel){
        this.Nivel=Nivel;
        this.Mote=Especie;
        this.Ataque=Nivel*2;
        this.Defensa=Nivel*3;
        this.Ps=Nivel*7;
        this.Especie="Bulbasaur";
        this.Tipo="Planta/Veneno";
    }
}